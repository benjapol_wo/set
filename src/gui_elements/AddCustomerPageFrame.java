package gui_elements;

import gui_backend.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddCustomerPageFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    public static String username = MainPageFrame.username;
    public static AddCustomerPageFrame frame;
    private JTextField nameField;
    private JTextField lastnameField;

    public static void main(String[] args) {
        frame = new AddCustomerPageFrame(username);
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (username.equals("invalid")) {
                    //TODO: Remove Comment : PageUtils.quitApp(-2);
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public AddCustomerPageFrame(String username) {
        setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
        setIconImage(Toolkit.getDefaultToolkit().getImage(AddCustomerPageFrame.class.getResource("/assets/windowIcon.png")));
        setResizable(false);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 450);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(UIManager.getColor("windowBorder"));
        menuPanel.setBounds(750, 0, 50, 450);
        contentPane.add(menuPanel);
        menuPanel.setLayout(null);

        JPanel menuItemPanel_1 = new JPanel();
        menuItemPanel_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openMainPage(MainPageFrame.username);
                AddCustomerPageFrame.frame.setVisible(false);
                AddCustomerPageFrame.frame.dispose();
            }
        });
        menuItemPanel_1.setToolTipText("Home");
        menuItemPanel_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_1.setBackground(Color.DARK_GRAY);
        menuItemPanel_1.setBounds(0, 0, 50, 88);
        menuPanel.add(menuItemPanel_1);
        menuItemPanel_1.setLayout(null);

        JLabel homeIconLabel = new JLabel("");
        homeIconLabel.setBounds(10, 29, 30, 30);
        homeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/homeIcon.png", 30, 30));
        menuItemPanel_1.add(homeIconLabel);

        JPanel menuItemPanel_2 = new JPanel();
        menuItemPanel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openSearchPage();
                AddCustomerPageFrame.frame.dispose();
            }
        });
        menuItemPanel_2.setToolTipText("Search");
        menuItemPanel_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_2.setBackground(Color.DARK_GRAY);
        menuItemPanel_2.setBounds(0, 90, 50, 88);
        menuPanel.add(menuItemPanel_2);
        menuItemPanel_2.setLayout(null);

        JLabel searchTimeIconLabel = new JLabel("");
        searchTimeIconLabel.setBounds(10, 29, 30, 30);
        searchTimeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        searchTimeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/searchTimeIcon.png", 30, 30));
        menuItemPanel_2.add(searchTimeIconLabel);

        JPanel menuItemPanel_3 = new JPanel();
        menuItemPanel_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCustomerPage();
                AddCustomerPageFrame.frame.setVisible(false);
                AddCustomerPageFrame.frame.dispose();
            }
        });
        menuItemPanel_3.setToolTipText("Customers");
        menuItemPanel_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_3.setBackground(new Color(0, 191, 255));
        menuItemPanel_3.setBounds(0, 180, 50, 88);
        menuPanel.add(menuItemPanel_3);
        menuItemPanel_3.setLayout(null);

        JLabel customerIconLabel = new JLabel("");
        customerIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_3.add(customerIconLabel);
        customerIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        customerIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/customerIcon.png", 30, 30));

        JPanel menuItemPanel_4 = new JPanel();
        menuItemPanel_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openBookingsPage();
                AddCustomerPageFrame.frame.dispose();
            }
        });
        menuItemPanel_4.setToolTipText("Bookings");
        menuItemPanel_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_4.setBackground(Color.DARK_GRAY);
        menuItemPanel_4.setBounds(0, 270, 50, 88);
        menuPanel.add(menuItemPanel_4);
        menuItemPanel_4.setLayout(null);

        JLabel billIconLabel = new JLabel("");
        billIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_4.add(billIconLabel);
        billIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        billIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/billIcon.png", 30, 30));

        JPanel menuItemPanel_5 = new JPanel();
        menuItemPanel_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openSettingsPage();
                AddCustomerPageFrame.frame.setVisible(false);
                AddCustomerPageFrame.frame.dispose();
            }
        });
        menuItemPanel_5.setToolTipText("Settings");
        menuItemPanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_5.setBackground(Color.DARK_GRAY);
        menuItemPanel_5.setBounds(0, 360, 50, 90);
        menuPanel.add(menuItemPanel_5);
        menuItemPanel_5.setLayout(null);

        JLabel settingsIconLabel = new JLabel("");
        settingsIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_5.add(settingsIconLabel);
        settingsIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        settingsIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/settingsIcon.png", 30, 30));

        JPanel mainPagePanel = new JPanel();
        mainPagePanel.setBackground(SystemColor.control);
        mainPagePanel.setBounds(0, 0, 750, 450);
        contentPane.add(mainPagePanel);
        mainPagePanel.setLayout(null);
        @SuppressWarnings({"unchecked", "rawtypes"})
        JComboBox genderComboBox = new JComboBox(new String[]{"M", "F"});
        genderComboBox.setBorder(new LineBorder(Color.DARK_GRAY, 2));
        genderComboBox.setBounds(110, 177, 160, 25);
        mainPagePanel.add(genderComboBox);

        JLabel nameLabel = new JLabel("name");
        nameLabel.setHorizontalAlignment(SwingConstants.LEFT);
        nameLabel.setForeground(SystemColor.controlDkShadow);
        nameLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
        nameLabel.setBounds(31, 105, 80, 25);
        mainPagePanel.add(nameLabel);

        nameField = new JTextField();
        nameField.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent arg0) {
                repaint();
            }
        });
        nameField.setSelectionColor(Color.LIGHT_GRAY);
        nameField.setForeground(Color.DARK_GRAY);
        nameField.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        nameField.setColumns(10);
        nameField.setBorder(new LineBorder(Color.DARK_GRAY, 2));
        nameField.setBackground(Color.WHITE);
        nameField.setBounds(110, 105, 160, 25);
        mainPagePanel.add(nameField);

        JLabel lastnameLabel = new JLabel("lastname");
        lastnameLabel.setHorizontalAlignment(SwingConstants.LEFT);
        lastnameLabel.setForeground(SystemColor.controlDkShadow);
        lastnameLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
        lastnameLabel.setBounds(31, 141, 80, 25);
        mainPagePanel.add(lastnameLabel);

        lastnameField = new JTextField();
        lastnameField.addCaretListener(new CaretListener() {
            public void caretUpdate(CaretEvent arg0) {
                repaint();
            }
        });
        lastnameField.setSelectionColor(Color.LIGHT_GRAY);
        lastnameField.setForeground(Color.DARK_GRAY);
        lastnameField.setFont(new Font("Segoe UI", Font.PLAIN, 14));
        lastnameField.setColumns(10);
        lastnameField.setBorder(new LineBorder(Color.DARK_GRAY, 2));
        lastnameField.setBackground(Color.WHITE);
        lastnameField.setBounds(110, 141, 160, 25);
        mainPagePanel.add(lastnameField);

        JLabel genderLabel = new JLabel("gender");
        genderLabel.setHorizontalAlignment(SwingConstants.LEFT);
        genderLabel.setForeground(SystemColor.controlDkShadow);
        genderLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));
        genderLabel.setBounds(31, 177, 80, 25);
        mainPagePanel.add(genderLabel);

        JButton addIconButton = new JButton("");
        addIconButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        addIconButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                repaint();
                int addRes = CustomerUtils.add(nameField.getText(), lastnameField.getText(), (String) genderComboBox.getSelectedItem());
                switch (addRes) {
                    case 0:
                        PageUtils.openCustomerPage();
                        PageUtils.showInfoDialog(InfoDialogFrame.DIALOG_NORMAL, "Customer successfully added.");
                        setVisible(false);
                        dispose();
                        break;
                    case -1:
                        PageUtils.showInfoDialog(InfoDialogFrame.DIALOG_ERROR, "Illegal customer name and/or lastname. Try again.");
                        break;
                    case -2:
                        PageUtils.showInfoDialog(InfoDialogFrame.DIALOG_NORMAL, "Can't add name or lastname that contains \"=\". Try again.");
                }
            }
        });
        JLabel addLabel = new JLabel("  Add");
        addLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        addLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addIconButton.doClick();
            }
        });
        addIconButton.setBorder(null);
        addIconButton.setBorderPainted(false);
        addIconButton.setIcon(ImageUtils.createScaledImageIcon(this, "assets/registerIcon.png", 50, 50));
        addIconButton.setBounds(32, 358, 50, 50);
        mainPagePanel.add(addIconButton);
        addLabel.setBackground(new Color(169, 169, 169));
        addLabel.setOpaque(true);
        addLabel.setForeground(SystemColor.controlDkShadow);
        addLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 20));
        addLabel.setBounds(82, 358, 60, 50);
        mainPagePanel.add(addLabel);

        JLabel logoLabel = new JLabel("");
        logoLabel.setBounds(21, 11, 88, 66);
        logoLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/logo.png", 88, 88));
        mainPagePanel.add(logoLabel);

        JPanel topBarPanel = new JPanel();
        topBarPanel.setBackground(Color.DARK_GRAY);
        topBarPanel.setBounds(0, 0, 750, 30);
        mainPagePanel.add(topBarPanel);
        topBarPanel.setLayout(null);

        JLabel backIconLabel = new JLabel("");
        backIconLabel.setToolTipText("Logout");
        backIconLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                DebugUtils.writeln(AddCustomerPageFrame.frame.getClass().toString().substring(6) + " | Logging out...");
                PageUtils.openLoginPage();
                AddCustomerPageFrame.frame.dispose();
            }
        });
        backIconLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        backIconLabel.setBounds(720, 0, 30, 30);
        topBarPanel.add(backIconLabel);
        backIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/redBackIcon.png", 30, 30));

        JLabel usernameLabel = new JLabel(" " + username);
        usernameLabel.setOpaque(true);
        usernameLabel.setBackground(new Color(128, 128, 128));
        usernameLabel.setForeground(new Color(220, 220, 220));
        usernameLabel.setBounds(620, 0, 130, 30);
        topBarPanel.add(usernameLabel);
        usernameLabel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));

        JLabel userIconLabel = new JLabel("");
        userIconLabel.setBounds(590, 0, 30, 30);
        topBarPanel.add(userIconLabel);
        userIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/userIcon.png", 30, 30));

        JLabel titleLabel = new JLabel("Add customer");
        titleLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 42));
        titleLabel.setForeground(new Color(105, 105, 105));
        titleLabel.setBounds(120, 18, 417, 66);
        mainPagePanel.add(titleLabel);

        setLocation(PositionUtils.getScreenCenterPoint(this));
    }
}
