package gui_elements;

import gui_backend.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CourtPageFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    public static String username = MainPageFrame.username;
    public static int courtNumber = 1;
    public static CourtPageFrame frame;

    public static void main(String[] args) {
        frame = new CourtPageFrame(username, courtNumber);
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (username.equals("invalid")) {
                    //TODO: Remove Comment : PageUtils.quitApp(-2);
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public CourtPageFrame(String username, int courtNumber) {
        setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
        setIconImage(Toolkit.getDefaultToolkit().getImage(CourtPageFrame.class.getResource("/assets/windowIcon.png")));
        setResizable(false);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 450);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(UIManager.getColor("windowBorder"));
        menuPanel.setBounds(750, 0, 50, 450);
        contentPane.add(menuPanel);
        menuPanel.setLayout(null);

        JPanel menuItemPanel_1 = new JPanel();
        menuItemPanel_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openMainPage(MainPageFrame.username);
                CourtPageFrame.frame.setVisible(false);
                CourtPageFrame.frame.dispose();
            }
        });
        menuItemPanel_1.setToolTipText("Home");
        menuItemPanel_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_1.setBackground(new Color(0, 191, 255));
        menuItemPanel_1.setBounds(0, 0, 50, 88);
        menuPanel.add(menuItemPanel_1);
        menuItemPanel_1.setLayout(null);

        JLabel homeIconLabel = new JLabel("");
        homeIconLabel.setBounds(10, 29, 30, 30);
        homeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/homeIcon.png", 30, 30));
        menuItemPanel_1.add(homeIconLabel);

        JPanel menuItemPanel_2 = new JPanel();
        menuItemPanel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openSearchPage();
                CourtPageFrame.frame.dispose();
            }
        });
        menuItemPanel_2.setToolTipText("Search");
        menuItemPanel_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_2.setBackground(Color.DARK_GRAY);
        menuItemPanel_2.setBounds(0, 90, 50, 88);
        menuPanel.add(menuItemPanel_2);
        menuItemPanel_2.setLayout(null);

        JLabel searchTimeIconLabel = new JLabel("");
        searchTimeIconLabel.setBounds(10, 29, 30, 30);
        searchTimeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        searchTimeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/searchTimeIcon.png", 30, 30));
        menuItemPanel_2.add(searchTimeIconLabel);

        JPanel menuItemPanel_3 = new JPanel();
        menuItemPanel_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCustomerPage();
                CourtPageFrame.frame.setVisible(false);
                CourtPageFrame.frame.dispose();
            }
        });
        menuItemPanel_3.setToolTipText("Customers");
        menuItemPanel_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_3.setBackground(Color.DARK_GRAY);
        menuItemPanel_3.setBounds(0, 180, 50, 88);
        menuPanel.add(menuItemPanel_3);
        menuItemPanel_3.setLayout(null);

        JLabel customerIconLabel = new JLabel("");
        customerIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_3.add(customerIconLabel);
        customerIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        customerIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/customerIcon.png", 30, 30));

        JPanel menuItemPanel_4 = new JPanel();
        menuItemPanel_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openBookingsPage();
                CourtPageFrame.frame.dispose();
            }
        });
        menuItemPanel_4.setToolTipText("Bookings");
        menuItemPanel_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_4.setBackground(Color.DARK_GRAY);
        menuItemPanel_4.setBounds(0, 270, 50, 88);
        menuPanel.add(menuItemPanel_4);
        menuItemPanel_4.setLayout(null);

        JLabel billIconLabel = new JLabel("");
        billIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_4.add(billIconLabel);
        billIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        billIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/billIcon.png", 30, 30));

        JPanel menuItemPanel_5 = new JPanel();
        menuItemPanel_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openSettingsPage();
                CourtPageFrame.frame.setVisible(false);
                CourtPageFrame.frame.dispose();
            }
        });
        menuItemPanel_5.setToolTipText("Settings");
        menuItemPanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_5.setBackground(Color.DARK_GRAY);
        menuItemPanel_5.setBounds(0, 360, 50, 90);
        menuPanel.add(menuItemPanel_5);
        menuItemPanel_5.setLayout(null);

        JLabel settingsIconLabel = new JLabel("");
        settingsIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_5.add(settingsIconLabel);
        settingsIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        settingsIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/settingsIcon.png", 30, 30));

        JPanel mainPagePanel = new JPanel();
        mainPagePanel.setBackground(SystemColor.control);
        mainPagePanel.setBounds(0, 0, 750, 450);
        contentPane.add(mainPagePanel);
        mainPagePanel.setLayout(null);

        JLabel logoLabel = new JLabel("");
        logoLabel.setBounds(21, 11, 88, 66);
        logoLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/logo.png", 88, 88));
        mainPagePanel.add(logoLabel);

        JPanel topBarPanel = new JPanel();
        topBarPanel.setBackground(Color.DARK_GRAY);
        topBarPanel.setBounds(0, 0, 750, 30);
        mainPagePanel.add(topBarPanel);
        topBarPanel.setLayout(null);

        JLabel backIconLabel = new JLabel("");
        backIconLabel.setToolTipText("Logout");
        backIconLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                DebugUtils.writeln(CourtPageFrame.frame.getClass().toString().substring(6) + " | Logging out...");
                PageUtils.openLoginPage();
                CourtPageFrame.frame.dispose();
            }
        });
        backIconLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        backIconLabel.setBounds(720, 0, 30, 30);
        topBarPanel.add(backIconLabel);
        backIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/redBackIcon.png", 30, 30));

        JLabel usernameLabel = new JLabel(" " + username);
        usernameLabel.setOpaque(true);
        usernameLabel.setBackground(new Color(128, 128, 128));
        usernameLabel.setForeground(new Color(220, 220, 220));
        usernameLabel.setBounds(620, 0, 130, 30);
        topBarPanel.add(usernameLabel);
        usernameLabel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));

        JLabel userIconLabel = new JLabel("");
        userIconLabel.setBounds(590, 0, 30, 30);
        topBarPanel.add(userIconLabel);
        userIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/userIcon.png", 30, 30));

        JLabel titleLabel = new JLabel("Select period");
        titleLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 42));
        titleLabel.setForeground(new Color(105, 105, 105));
        titleLabel.setBounds(120, 18, 417, 66);
        mainPagePanel.add(titleLabel);

        JLabel lblCourt = new JLabel("COURT " + courtNumber);
        lblCourt.setVerticalAlignment(SwingConstants.BOTTOM);
        lblCourt.setForeground(SystemColor.controlDkShadow);
        lblCourt.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 30));
        lblCourt.setBounds(31, 100, 136, 35);
        mainPagePanel.add(lblCourt);

        JLabel label_0 = new JLabel("08:00-09:00");
        label_0.setHorizontalAlignment(SwingConstants.CENTER);
        label_0.setBackground(new Color(105, 105, 105));
        label_0.setOpaque(true);
        label_0.setVerticalAlignment(SwingConstants.BOTTOM);
        label_0.setForeground(new Color(245, 245, 245));
        label_0.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_0.setBounds(41, 146, 126, 30);
        mainPagePanel.add(label_0);

        JLabel label_1 = new JLabel("09:00-10:00");
        label_1.setHorizontalAlignment(SwingConstants.CENTER);
        label_1.setVerticalAlignment(SwingConstants.BOTTOM);
        label_1.setOpaque(true);
        label_1.setForeground(new Color(245, 245, 245));
        label_1.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_1.setBackground(new Color(105, 105, 105));
        label_1.setBounds(41, 187, 126, 30);
        mainPagePanel.add(label_1);

        JLabel label_2 = new JLabel("10:00-11:00");
        label_2.setHorizontalAlignment(SwingConstants.CENTER);
        label_2.setVerticalAlignment(SwingConstants.BOTTOM);
        label_2.setOpaque(true);
        label_2.setForeground(new Color(245, 245, 245));
        label_2.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_2.setBackground(new Color(105, 105, 105));
        label_2.setBounds(41, 228, 126, 30);
        mainPagePanel.add(label_2);

        JLabel label_3 = new JLabel("11:00-12:00");
        label_3.setHorizontalAlignment(SwingConstants.CENTER);
        label_3.setVerticalAlignment(SwingConstants.BOTTOM);
        label_3.setOpaque(true);
        label_3.setForeground(new Color(245, 245, 245));
        label_3.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_3.setBackground(new Color(105, 105, 105));
        label_3.setBounds(41, 269, 126, 30);
        mainPagePanel.add(label_3);

        JLabel label_4 = new JLabel("12:00-13:00");
        label_4.setHorizontalAlignment(SwingConstants.CENTER);
        label_4.setVerticalAlignment(SwingConstants.BOTTOM);
        label_4.setOpaque(true);
        label_4.setForeground(new Color(245, 245, 245));
        label_4.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_4.setBackground(new Color(105, 105, 105));
        label_4.setBounds(41, 310, 126, 30);
        mainPagePanel.add(label_4);

        JLabel label_5 = new JLabel("13:00-14:00");
        label_5.setHorizontalAlignment(SwingConstants.CENTER);
        label_5.setVerticalAlignment(SwingConstants.BOTTOM);
        label_5.setOpaque(true);
        label_5.setForeground(new Color(245, 245, 245));
        label_5.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_5.setBackground(new Color(105, 105, 105));
        label_5.setBounds(41, 351, 126, 30);
        mainPagePanel.add(label_5);

        JLabel label_6 = new JLabel("14:00-15:00");
        label_6.setHorizontalAlignment(SwingConstants.CENTER);
        label_6.setVerticalAlignment(SwingConstants.BOTTOM);
        label_6.setOpaque(true);
        label_6.setForeground(new Color(245, 245, 245));
        label_6.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_6.setBackground(new Color(105, 105, 105));
        label_6.setBounds(384, 146, 126, 30);
        mainPagePanel.add(label_6);

        JLabel label_7 = new JLabel("15:00-16:00");
        label_7.setHorizontalAlignment(SwingConstants.CENTER);
        label_7.setVerticalAlignment(SwingConstants.BOTTOM);
        label_7.setOpaque(true);
        label_7.setForeground(new Color(245, 245, 245));
        label_7.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_7.setBackground(new Color(105, 105, 105));
        label_7.setBounds(384, 187, 126, 30);
        mainPagePanel.add(label_7);

        JLabel label_8 = new JLabel("16:00-17:00");
        label_8.setHorizontalAlignment(SwingConstants.CENTER);
        label_8.setVerticalAlignment(SwingConstants.BOTTOM);
        label_8.setOpaque(true);
        label_8.setForeground(new Color(245, 245, 245));
        label_8.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_8.setBackground(new Color(105, 105, 105));
        label_8.setBounds(384, 228, 126, 30);
        mainPagePanel.add(label_8);

        JLabel label_9 = new JLabel("17:00-18:00");
        label_9.setHorizontalAlignment(SwingConstants.CENTER);
        label_9.setVerticalAlignment(SwingConstants.BOTTOM);
        label_9.setOpaque(true);
        label_9.setForeground(new Color(245, 245, 245));
        label_9.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_9.setBackground(new Color(105, 105, 105));
        label_9.setBounds(384, 269, 126, 30);
        mainPagePanel.add(label_9);

        JLabel label_10 = new JLabel("18:00-19:00");
        label_10.setHorizontalAlignment(SwingConstants.CENTER);
        label_10.setVerticalAlignment(SwingConstants.BOTTOM);
        label_10.setOpaque(true);
        label_10.setForeground(new Color(245, 245, 245));
        label_10.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_10.setBackground(new Color(105, 105, 105));
        label_10.setBounds(384, 310, 126, 30);
        mainPagePanel.add(label_10);

        JLabel label_11 = new JLabel("19:00-20:00");
        label_11.setHorizontalAlignment(SwingConstants.CENTER);
        label_11.setVerticalAlignment(SwingConstants.BOTTOM);
        label_11.setOpaque(true);
        label_11.setForeground(new Color(245, 245, 245));
        label_11.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        label_11.setBackground(new Color(105, 105, 105));
        label_11.setBounds(384, 351, 126, 30);
        mainPagePanel.add(label_11);

        JLabel status_0 = new JLabel("status");
        status_0.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 0);
                setVisible(false);
                dispose();
            }
        });
        status_0.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_0.setText(CourtUtils.getStatusString(courtNumber, 0));
        status_0.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_0.setVerticalAlignment(SwingConstants.BOTTOM);
        status_0.setHorizontalAlignment(SwingConstants.CENTER);
        status_0.setForeground(new Color(105, 105, 105));
        status_0.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_0.setBackground(SystemColor.controlDkShadow);
        status_0.setBounds(165, 146, 126, 30);
        mainPagePanel.add(status_0);

        JLabel status_1 = new JLabel("status");
        status_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 1);
                setVisible(false);
                dispose();
            }
        });
        status_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_1.setText(CourtUtils.getStatusString(courtNumber, 1));
        status_1.setVerticalAlignment(SwingConstants.BOTTOM);
        status_1.setHorizontalAlignment(SwingConstants.CENTER);
        status_1.setForeground(SystemColor.controlDkShadow);
        status_1.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_1.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_1.setBackground(SystemColor.controlDkShadow);
        status_1.setBounds(165, 187, 126, 30);
        mainPagePanel.add(status_1);

        JLabel status_2 = new JLabel("status");
        status_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 2);
                setVisible(false);
                dispose();
            }
        });
        status_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_2.setText(CourtUtils.getStatusString(courtNumber, 2));
        status_2.setVerticalAlignment(SwingConstants.BOTTOM);
        status_2.setHorizontalAlignment(SwingConstants.CENTER);
        status_2.setForeground(SystemColor.controlDkShadow);
        status_2.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_2.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_2.setBackground(SystemColor.controlDkShadow);
        status_2.setBounds(165, 228, 126, 30);
        mainPagePanel.add(status_2);

        JLabel status_3 = new JLabel("status");
        status_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 3);
                setVisible(false);
                dispose();
            }
        });
        status_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_3.setText(CourtUtils.getStatusString(courtNumber, 3));
        status_3.setVerticalAlignment(SwingConstants.BOTTOM);
        status_3.setHorizontalAlignment(SwingConstants.CENTER);
        status_3.setForeground(SystemColor.controlDkShadow);
        status_3.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_3.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_3.setBackground(SystemColor.controlDkShadow);
        status_3.setBounds(165, 269, 126, 30);
        mainPagePanel.add(status_3);

        JLabel status_4 = new JLabel("status");
        status_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 4);
                setVisible(false);
                dispose();
            }
        });
        status_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_4.setText(CourtUtils.getStatusString(courtNumber, 4));
        status_4.setVerticalAlignment(SwingConstants.BOTTOM);
        status_4.setHorizontalAlignment(SwingConstants.CENTER);
        status_4.setForeground(SystemColor.controlDkShadow);
        status_4.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_4.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_4.setBackground(SystemColor.controlDkShadow);
        status_4.setBounds(165, 310, 126, 30);
        mainPagePanel.add(status_4);

        JLabel status_5 = new JLabel("status");
        status_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 5);
                setVisible(false);
                dispose();
            }
        });
        status_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_5.setText(CourtUtils.getStatusString(courtNumber, 5));
        status_5.setVerticalAlignment(SwingConstants.BOTTOM);
        status_5.setHorizontalAlignment(SwingConstants.CENTER);
        status_5.setForeground(SystemColor.controlDkShadow);
        status_5.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_5.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_5.setBackground(SystemColor.controlDkShadow);
        status_5.setBounds(165, 351, 126, 30);
        mainPagePanel.add(status_5);

        JLabel status_6 = new JLabel("status");
        status_6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 6);
                setVisible(false);
                dispose();
            }
        });
        status_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_6.setText(CourtUtils.getStatusString(courtNumber, 6));
        status_6.setVerticalAlignment(SwingConstants.BOTTOM);
        status_6.setHorizontalAlignment(SwingConstants.CENTER);
        status_6.setForeground(SystemColor.controlDkShadow);
        status_6.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_6.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_6.setBackground(SystemColor.controlDkShadow);
        status_6.setBounds(510, 146, 126, 30);
        mainPagePanel.add(status_6);

        JLabel status_7 = new JLabel("status");
        status_7.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 7);
                setVisible(false);
                dispose();
            }
        });
        status_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_7.setText(CourtUtils.getStatusString(courtNumber, 7));
        status_7.setVerticalAlignment(SwingConstants.BOTTOM);
        status_7.setHorizontalAlignment(SwingConstants.CENTER);
        status_7.setForeground(SystemColor.controlDkShadow);
        status_7.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_7.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_7.setBackground(SystemColor.controlDkShadow);
        status_7.setBounds(510, 187, 126, 30);
        mainPagePanel.add(status_7);

        JLabel status_8 = new JLabel("status");
        status_8.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 8);
                setVisible(false);
                dispose();
            }
        });
        status_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_8.setText(CourtUtils.getStatusString(courtNumber, 8));
        status_8.setVerticalAlignment(SwingConstants.BOTTOM);
        status_8.setHorizontalAlignment(SwingConstants.CENTER);
        status_8.setForeground(SystemColor.controlDkShadow);
        status_8.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_8.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_8.setBackground(SystemColor.controlDkShadow);
        status_8.setBounds(510, 228, 126, 30);
        mainPagePanel.add(status_8);

        JLabel status_9 = new JLabel("status");
        status_9.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 9);
                setVisible(false);
                dispose();
            }
        });
        status_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_9.setText(CourtUtils.getStatusString(courtNumber, 9));
        status_9.setVerticalAlignment(SwingConstants.BOTTOM);
        status_9.setHorizontalAlignment(SwingConstants.CENTER);
        status_9.setForeground(SystemColor.controlDkShadow);
        status_9.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_9.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_9.setBackground(SystemColor.controlDkShadow);
        status_9.setBounds(510, 269, 126, 30);
        mainPagePanel.add(status_9);

        JLabel status_10 = new JLabel("status");
        status_10.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 10);
                setVisible(false);
                dispose();
            }
        });
        status_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_10.setText(CourtUtils.getStatusString(courtNumber, 10));
        status_10.setVerticalAlignment(SwingConstants.BOTTOM);
        status_10.setHorizontalAlignment(SwingConstants.CENTER);
        status_10.setForeground(SystemColor.controlDkShadow);
        status_10.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_10.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_10.setBackground(SystemColor.controlDkShadow);
        status_10.setBounds(510, 310, 126, 30);
        mainPagePanel.add(status_10);

        JLabel status_11 = new JLabel("status");
        status_11.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openAddBookingPage(courtNumber, 11);
                setVisible(false);
                dispose();
            }
        });
        status_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        status_11.setText(CourtUtils.getStatusString(courtNumber, 11));
        status_11.setVerticalAlignment(SwingConstants.BOTTOM);
        status_11.setHorizontalAlignment(SwingConstants.CENTER);
        status_11.setForeground(SystemColor.controlDkShadow);
        status_11.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 22));
        status_11.setBorder(new LineBorder(new Color(105, 105, 105), 2));
        status_11.setBackground(SystemColor.controlDkShadow);
        status_11.setBounds(510, 351, 126, 30);
        mainPagePanel.add(status_11);

        setLocation(PositionUtils.getScreenCenterPoint(this));
    }
}
