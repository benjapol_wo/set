package gui_elements;

import gui_backend.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BookingPageFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    public static String username = MainPageFrame.username;
    public static BookingPageFrame frame;

    public static void main(String[] args) {
        frame = new BookingPageFrame(username);
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (username.equals("invalid")) {
                    //TODO: Remove Comment : PageUtils.quitApp(-2);
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public BookingPageFrame(String username) {
        setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
        setIconImage(Toolkit.getDefaultToolkit().getImage(BookingPageFrame.class.getResource("/assets/windowIcon.png")));
        setResizable(false);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 450);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(UIManager.getColor("windowBorder"));
        menuPanel.setBounds(750, 0, 50, 450);
        contentPane.add(menuPanel);
        menuPanel.setLayout(null);

        JPanel menuItemPanel_1 = new JPanel();
        menuItemPanel_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openMainPage(MainPageFrame.username);
                BookingPageFrame.frame.setVisible(false);
                BookingPageFrame.frame.dispose();
            }
        });
        menuItemPanel_1.setToolTipText("Home");
        menuItemPanel_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_1.setBackground(Color.DARK_GRAY);
        menuItemPanel_1.setBounds(0, 0, 50, 88);
        menuPanel.add(menuItemPanel_1);
        menuItemPanel_1.setLayout(null);

        JLabel homeIconLabel = new JLabel("");
        homeIconLabel.setBounds(10, 29, 30, 30);
        homeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/homeIcon.png", 30, 30));
        menuItemPanel_1.add(homeIconLabel);

        JPanel menuItemPanel_2 = new JPanel();
        menuItemPanel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openSearchPage();
                BookingPageFrame.frame.dispose();
            }
        });
        menuItemPanel_2.setToolTipText("Search");
        menuItemPanel_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_2.setBackground(Color.DARK_GRAY);
        menuItemPanel_2.setBounds(0, 90, 50, 88);
        menuPanel.add(menuItemPanel_2);
        menuItemPanel_2.setLayout(null);

        JLabel searchTimeIconLabel = new JLabel("");
        searchTimeIconLabel.setBounds(10, 29, 30, 30);
        searchTimeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        searchTimeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/searchTimeIcon.png", 30, 30));
        menuItemPanel_2.add(searchTimeIconLabel);

        JPanel menuItemPanel_3 = new JPanel();
        menuItemPanel_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCustomerPage();
                BookingPageFrame.frame.setVisible(false);
                BookingPageFrame.frame.dispose();
            }
        });
        menuItemPanel_3.setToolTipText("Customers");
        menuItemPanel_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_3.setBackground(Color.DARK_GRAY);
        menuItemPanel_3.setBounds(0, 180, 50, 88);
        menuPanel.add(menuItemPanel_3);
        menuItemPanel_3.setLayout(null);

        JLabel customerIconLabel = new JLabel("");
        customerIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_3.add(customerIconLabel);
        customerIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        customerIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/customerIcon.png", 30, 30));

        JPanel menuItemPanel_4 = new JPanel();
        menuItemPanel_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openBookingsPage();
                BookingPageFrame.frame.dispose();
            }
        });
        menuItemPanel_4.setToolTipText("Bookings");
        menuItemPanel_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_4.setBackground(new Color(0, 191, 255));
        menuItemPanel_4.setBounds(0, 270, 50, 88);
        menuPanel.add(menuItemPanel_4);
        menuItemPanel_4.setLayout(null);

        JLabel billIconLabel = new JLabel("");
        billIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_4.add(billIconLabel);
        billIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        billIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/billIcon.png", 30, 30));

        JPanel menuItemPanel_5 = new JPanel();
        menuItemPanel_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openSettingsPage();
                BookingPageFrame.frame.setVisible(false);
                BookingPageFrame.frame.dispose();
            }
        });
        menuItemPanel_5.setToolTipText("Settings");
        menuItemPanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_5.setBackground(Color.DARK_GRAY);
        menuItemPanel_5.setBounds(0, 360, 50, 90);
        menuPanel.add(menuItemPanel_5);
        menuItemPanel_5.setLayout(null);

        JLabel settingsIconLabel = new JLabel("");
        settingsIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_5.add(settingsIconLabel);
        settingsIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        settingsIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/settingsIcon.png", 30, 30));

        JPanel mainPagePanel = new JPanel();
        mainPagePanel.setBackground(SystemColor.control);
        mainPagePanel.setBounds(0, 0, 750, 450);
        contentPane.add(mainPagePanel);
        mainPagePanel.setLayout(null);

        JLabel logoLabel = new JLabel("");
        logoLabel.setBounds(21, 11, 88, 66);
        logoLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/logo.png", 88, 88));
        mainPagePanel.add(logoLabel);

        JPanel topBarPanel = new JPanel();
        topBarPanel.setBackground(Color.DARK_GRAY);
        topBarPanel.setBounds(0, 0, 750, 30);
        mainPagePanel.add(topBarPanel);
        topBarPanel.setLayout(null);

        JLabel backIconLabel = new JLabel("");
        backIconLabel.setToolTipText("Logout");
        backIconLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                DebugUtils.writeln(BookingPageFrame.frame.getClass().toString().substring(6) + " | Logging out...");
                PageUtils.openLoginPage();
                BookingPageFrame.frame.dispose();
            }
        });
        backIconLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        backIconLabel.setBounds(720, 0, 30, 30);
        topBarPanel.add(backIconLabel);
        backIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/redBackIcon.png", 30, 30));

        JLabel usernameLabel = new JLabel(" " + username);
        usernameLabel.setOpaque(true);
        usernameLabel.setBackground(new Color(128, 128, 128));
        usernameLabel.setForeground(new Color(220, 220, 220));
        usernameLabel.setBounds(620, 0, 130, 30);
        topBarPanel.add(usernameLabel);
        usernameLabel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));

        JLabel userIconLabel = new JLabel("");
        userIconLabel.setBounds(590, 0, 30, 30);
        topBarPanel.add(userIconLabel);
        userIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/userIcon.png", 30, 30));

        JLabel titleLabel = new JLabel("Bookings");
        titleLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 42));
        titleLabel.setForeground(new Color(105, 105, 105));
        titleLabel.setBounds(120, 18, 417, 66);
        mainPagePanel.add(titleLabel);

        JTextPane textPane = new JTextPane();
        textPane.setText(BookingUtils.getViewMode());
        textPane.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
        textPane.setEditable(false);
        textPane.setBounds(31, 100, 684, 327);
        mainPagePanel.add(textPane);

        setLocation(PositionUtils.getScreenCenterPoint(this));
    }
}
