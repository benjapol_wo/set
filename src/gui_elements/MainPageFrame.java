package gui_elements;

import gui_backend.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainPageFrame extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    public static String username = "invalid";
    public static MainPageFrame frame;

    public static void main(String[] args) {
        frame = new MainPageFrame(username);
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (username.equals("invalid")) {
                    //TODO: Remove Comment : PageUtils.quitApp(-2);
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public MainPageFrame(String username) {
        setFont(new Font("Segoe UI Semilight", Font.PLAIN, 12));
        setIconImage(Toolkit.getDefaultToolkit().getImage(MainPageFrame.class.getResource("/assets/windowIcon.png")));
        setResizable(false);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 800, 450);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel menuPanel = new JPanel();
        menuPanel.setBackground(UIManager.getColor("windowBorder"));
        menuPanel.setBounds(750, 0, 50, 450);
        contentPane.add(menuPanel);
        menuPanel.setLayout(null);

        JPanel menuItemPanel_1 = new JPanel();
        menuItemPanel_1.setToolTipText("Home");
        menuItemPanel_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_1.setBackground(new Color(0, 191, 255));
        menuItemPanel_1.setBounds(0, 0, 50, 88);
        menuPanel.add(menuItemPanel_1);
        menuItemPanel_1.setLayout(null);

        JLabel homeIconLabel = new JLabel("");
        homeIconLabel.setBounds(10, 29, 30, 30);
        homeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        homeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/homeIcon.png", 30, 30));
        menuItemPanel_1.add(homeIconLabel);

        JPanel menuItemPanel_2 = new JPanel();
        menuItemPanel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                //PageUtils.openSearchPage();
                MainPageFrame.frame.dispose();
            }
        });
        menuItemPanel_2.setToolTipText("Search");
        menuItemPanel_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_2.setBackground(Color.DARK_GRAY);
        menuItemPanel_2.setBounds(0, 90, 50, 88);
        menuPanel.add(menuItemPanel_2);
        menuItemPanel_2.setLayout(null);

        JLabel searchTimeIconLabel = new JLabel("");
        searchTimeIconLabel.setBounds(10, 29, 30, 30);
        searchTimeIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        searchTimeIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/searchTimeIcon.png", 30, 30));
        menuItemPanel_2.add(searchTimeIconLabel);

        JPanel menuItemPanel_3 = new JPanel();
        menuItemPanel_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCustomerPage();
                MainPageFrame.frame.dispose();
            }
        });
        menuItemPanel_3.setToolTipText("Customers");
        menuItemPanel_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_3.setBackground(Color.DARK_GRAY);
        menuItemPanel_3.setBounds(0, 180, 50, 88);
        menuPanel.add(menuItemPanel_3);
        menuItemPanel_3.setLayout(null);

        JLabel customerIconLabel = new JLabel("");
        customerIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_3.add(customerIconLabel);
        customerIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        customerIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/customerIcon.png", 30, 30));

        JPanel menuItemPanel_4 = new JPanel();
        menuItemPanel_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openBookingPage();
                MainPageFrame.frame.setVisible(false);
                MainPageFrame.frame.dispose();
            }
        });
        menuItemPanel_4.setToolTipText("Bookings");
        menuItemPanel_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_4.setBackground(Color.DARK_GRAY);
        menuItemPanel_4.setBounds(0, 270, 50, 88);
        menuPanel.add(menuItemPanel_4);
        menuItemPanel_4.setLayout(null);

        JLabel billIconLabel = new JLabel("");
        billIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_4.add(billIconLabel);
        billIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        billIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/billIcon.png", 30, 30));

        JPanel menuItemPanel_5 = new JPanel();
        menuItemPanel_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openSettingsPage();
                MainPageFrame.frame.dispose();
            }
        });
        menuItemPanel_5.setToolTipText("Settings");
        menuItemPanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        menuItemPanel_5.setBackground(Color.DARK_GRAY);
        menuItemPanel_5.setBounds(0, 360, 50, 90);
        menuPanel.add(menuItemPanel_5);
        menuItemPanel_5.setLayout(null);

        JLabel settingsIconLabel = new JLabel("");
        settingsIconLabel.setBounds(10, 29, 30, 30);
        menuItemPanel_5.add(settingsIconLabel);
        settingsIconLabel.setHorizontalAlignment(SwingConstants.CENTER);
        settingsIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/settingsIcon.png", 30, 30));

        JPanel mainPagePanel = new JPanel();
        mainPagePanel.setBackground(SystemColor.control);
        mainPagePanel.setBounds(0, 0, 750, 450);
        contentPane.add(mainPagePanel);
        mainPagePanel.setLayout(null);

        JLabel titleLabel = new JLabel("Courts");
        titleLabel.setVerticalAlignment(SwingConstants.BOTTOM);
        titleLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 42));
        titleLabel.setForeground(new Color(105, 105, 105));
        titleLabel.setBounds(120, 18, 283, 66);
        mainPagePanel.add(titleLabel);

        JLabel logoLabel = new JLabel("");
        logoLabel.setBounds(21, 11, 88, 66);
        logoLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/logo.png", 88, 88));
        mainPagePanel.add(logoLabel);

        JPanel topBarPanel = new JPanel();
        topBarPanel.setBackground(Color.DARK_GRAY);
        topBarPanel.setBounds(0, 0, 750, 30);
        mainPagePanel.add(topBarPanel);
        topBarPanel.setLayout(null);

        JLabel backIconLabel = new JLabel("");
        backIconLabel.setToolTipText("Logout");
        backIconLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                DebugUtils.writeln(MainPageFrame.frame.getClass().toString().substring(6) + " | Logging out...");
                PageUtils.openLoginPage();
                MainPageFrame.frame.dispose();
            }
        });
        backIconLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        backIconLabel.setBounds(720, 0, 30, 30);
        topBarPanel.add(backIconLabel);
        backIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/redBackIcon.png", 30, 30));

        JLabel usernameLabel = new JLabel(" " + username);
        usernameLabel.setOpaque(true);
        usernameLabel.setBackground(new Color(128, 128, 128));
        usernameLabel.setForeground(new Color(220, 220, 220));
        usernameLabel.setBounds(620, 0, 130, 30);
        topBarPanel.add(usernameLabel);
        usernameLabel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));

        JLabel userIconLabel = new JLabel("");
        userIconLabel.setBounds(590, 0, 30, 30);
        topBarPanel.add(userIconLabel);
        userIconLabel.setIcon(ImageUtils.createScaledImageIcon(this, "assets/userIcon.png", 30, 30));

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setBorder(null);
        scrollPane.setBounds(20, 88, 730, 362);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(10);
        mainPagePanel.add(scrollPane);

        JPanel courtStatusPanel = new JPanel();
        scrollPane.setViewportView(courtStatusPanel);
        courtStatusPanel.setLayout(null);
        courtStatusPanel.setSize(1690, 345);
        courtStatusPanel.setMinimumSize(new Dimension(1690, 345));
        courtStatusPanel.setPreferredSize(new Dimension(1690, 345));

        JPanel courtPanel_1 = new JPanel();
        courtPanel_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(1);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_1.setBackground(new Color(245, 245, 245));
        courtPanel_1.setBounds(0, 0, 200, 345);
        courtStatusPanel.add(courtPanel_1);
        courtPanel_1.setLayout(null);

        JPanel courtDisplayPanel_1 = new JPanel();
        courtDisplayPanel_1.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_1.setBounds(0, 265, 200, 80);
        courtPanel_1.add(courtDisplayPanel_1);
        courtDisplayPanel_1.setLayout(null);

        JLabel courtLabel_1 = new JLabel("COURT 1");
        courtLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_1.setBounds(10, 11, 85, 18);
        courtDisplayPanel_1.add(courtLabel_1);
        courtLabel_1.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_1.setForeground(SystemColor.controlDkShadow);
        courtLabel_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_1 = new JLabel("");
        courtPanelBg_1.setBounds(0, 0, 200, 345);
        courtPanelBg_1.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg1.png"));
        courtPanel_1.add(courtPanelBg_1);

        JPanel courtPanel_2 = new JPanel();
        courtPanel_2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(2);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_2.setBackground(new Color(245, 245, 245));
        courtPanel_2.setLayout(null);
        courtPanel_2.setBounds(210, 0, 200, 345);
        courtStatusPanel.add(courtPanel_2);

        JPanel courtDisplayPanel_2 = new JPanel();
        courtDisplayPanel_2.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_2.setBounds(0, 265, 200, 80);
        courtPanel_2.add(courtDisplayPanel_2);
        courtDisplayPanel_2.setLayout(null);

        JLabel courtLabel_2 = new JLabel("COURT 2");
        courtLabel_2.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_2.setBounds(10, 11, 85, 18);
        courtDisplayPanel_2.add(courtLabel_2);
        courtLabel_2.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_2.setForeground(SystemColor.controlDkShadow);
        courtLabel_2.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_2 = new JLabel("");
        courtPanelBg_2.setBounds(0, 0, 200, 345);
        courtPanelBg_2.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg2.png"));
        courtPanel_2.add(courtPanelBg_2);

        JPanel courtPanel_3 = new JPanel();
        courtPanel_3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_3.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(3);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_3.setBackground(new Color(245, 245, 245));
        courtPanel_3.setLayout(null);
        courtPanel_3.setBounds(420, 0, 200, 345);
        courtStatusPanel.add(courtPanel_3);

        JPanel courtDisplayPanel_3 = new JPanel();
        courtDisplayPanel_3.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_3.setBounds(0, 265, 200, 80);
        courtPanel_3.add(courtDisplayPanel_3);
        courtDisplayPanel_3.setLayout(null);

        JLabel courtLabel_3 = new JLabel("COURT 3");
        courtLabel_3.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_3.setBounds(10, 11, 85, 18);
        courtDisplayPanel_3.add(courtLabel_3);
        courtLabel_3.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_3.setForeground(SystemColor.controlDkShadow);
        courtLabel_3.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_3 = new JLabel("");
        courtPanelBg_3.setBounds(0, 0, 200, 345);
        courtPanelBg_3.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg3.png"));
        courtPanel_3.add(courtPanelBg_3);

        JPanel courtPanel_4 = new JPanel();
        courtPanel_4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_4.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(4);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_4.setBackground(new Color(245, 245, 245));
        courtPanel_4.setLayout(null);
        courtPanel_4.setBounds(630, 0, 200, 345);
        courtStatusPanel.add(courtPanel_4);

        JPanel courtDisplayPanel_4 = new JPanel();
        courtDisplayPanel_4.setBounds(0, 265, 200, 80);
        courtPanel_4.add(courtDisplayPanel_4);
        courtDisplayPanel_4.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_4.setLayout(null);

        JLabel courtLabel_4 = new JLabel("COURT 4");
        courtLabel_4.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_4.setBounds(10, 11, 85, 18);
        courtDisplayPanel_4.add(courtLabel_4);
        courtLabel_4.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_4.setForeground(SystemColor.controlDkShadow);
        courtLabel_4.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_4 = new JLabel("");
        courtPanelBg_4.setBounds(0, 0, 200, 345);
        courtPanel_4.add(courtPanelBg_4);
        courtPanelBg_4.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg4.png"));

        JPanel courtPanel_5 = new JPanel();
        courtPanel_5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_5.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(5);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_5.setLayout(null);
        courtPanel_5.setBackground(new Color(245, 245, 245));
        courtPanel_5.setBounds(840, 0, 200, 345);
        courtStatusPanel.add(courtPanel_5);

        JPanel courtDisplayPanel_5 = new JPanel();
        courtDisplayPanel_5.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_5.setBounds(0, 265, 200, 80);
        courtPanel_5.add(courtDisplayPanel_5);
        courtDisplayPanel_5.setLayout(null);

        JLabel courtLabel_5 = new JLabel("COURT 5");
        courtLabel_5.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_5.setBounds(10, 11, 85, 18);
        courtDisplayPanel_5.add(courtLabel_5);
        courtLabel_5.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_5.setForeground(SystemColor.controlDkShadow);
        courtLabel_5.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_5 = new JLabel("");
        courtPanelBg_5.setBounds(0, 0, 200, 345);
        courtPanelBg_5.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg5.png"));
        courtPanel_5.add(courtPanelBg_5);

        JPanel courtPanel_6 = new JPanel();
        courtPanel_6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_6.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(6);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_6.setLayout(null);
        courtPanel_6.setBackground(new Color(245, 245, 245));
        courtPanel_6.setBounds(1050, 0, 200, 345);
        courtStatusPanel.add(courtPanel_6);

        JPanel courtDisplayPanel_6 = new JPanel();
        courtDisplayPanel_6.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_6.setBounds(0, 265, 200, 80);
        courtPanel_6.add(courtDisplayPanel_6);
        courtDisplayPanel_6.setLayout(null);

        JLabel courtLabel_6 = new JLabel("COURT 6");
        courtLabel_6.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_6.setBounds(10, 11, 85, 18);
        courtDisplayPanel_6.add(courtLabel_6);
        courtLabel_6.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_6.setForeground(SystemColor.controlDkShadow);
        courtLabel_6.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_6 = new JLabel("");
        courtPanelBg_6.setBounds(0, 0, 210, 345);
        courtPanelBg_6.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg6.png"));
        courtPanel_6.add(courtPanelBg_6);

        JPanel courtPanel_7 = new JPanel();
        courtPanel_7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_7.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(7);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_7.setLayout(null);
        courtPanel_7.setBackground(new Color(245, 245, 245));
        courtPanel_7.setBounds(1260, 0, 200, 345);
        courtStatusPanel.add(courtPanel_7);

        JPanel courtDisplayPanel_7 = new JPanel();
        courtDisplayPanel_7.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_7.setBounds(0, 265, 200, 80);
        courtPanel_7.add(courtDisplayPanel_7);
        courtDisplayPanel_7.setLayout(null);

        JLabel courtLabel_7 = new JLabel("COURT 7");
        courtLabel_7.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_7.setBounds(10, 11, 85, 18);
        courtDisplayPanel_7.add(courtLabel_7);
        courtLabel_7.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_7.setForeground(SystemColor.controlDkShadow);
        courtLabel_7.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_7 = new JLabel("");
        courtPanelBg_7.setBounds(0, 0, 210, 345);
        courtPanelBg_7.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg7.png"));
        courtPanel_7.add(courtPanelBg_7);

        JPanel courtPanel_8 = new JPanel();
        courtPanel_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        courtPanel_8.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                PageUtils.openCourtPage(8);
                setVisible(false);
                dispose();
            }
        });
        courtPanel_8.setLayout(null);
        courtPanel_8.setBackground(new Color(245, 245, 245));
        courtPanel_8.setBounds(1470, 0, 200, 345);
        courtStatusPanel.add(courtPanel_8);

        JPanel courtDisplayPanel_8 = new JPanel();
        courtDisplayPanel_8.setBackground(new Color(245, 245, 245, 180));
        courtDisplayPanel_8.setBounds(0, 265, 200, 80);
        courtPanel_8.add(courtDisplayPanel_8);
        courtDisplayPanel_8.setLayout(null);

        JLabel courtLabel_8 = new JLabel("COURT 8");
        courtLabel_8.setHorizontalAlignment(SwingConstants.LEFT);
        courtLabel_8.setBounds(10, 11, 85, 18);
        courtDisplayPanel_8.add(courtLabel_8);
        courtLabel_8.setVerticalAlignment(SwingConstants.BOTTOM);
        courtLabel_8.setForeground(SystemColor.controlDkShadow);
        courtLabel_8.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 18));

        JLabel courtPanelBg_8 = new JLabel("");
        courtPanelBg_8.setBounds(0, 0, 210, 345);
        courtPanelBg_8.setIcon(ImageUtils.createImageIcon(this, "assets/panelbg8.png"));
        courtPanel_8.add(courtPanelBg_8);

        JLabel label = new JLabel(Double.toString(BookingUtils.getProfit()));
        label.setForeground(Color.DARK_GRAY);
        label.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
        label.setBackground(Color.DARK_GRAY);
        label.setBounds(695, 29, 130, 30);
        mainPagePanel.add(label);

        JLabel lblCurrentProfit = new JLabel("current profit:");
        lblCurrentProfit.setForeground(Color.DARK_GRAY);
        lblCurrentProfit.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
        lblCurrentProfit.setBackground(Color.DARK_GRAY);
        lblCurrentProfit.setBounds(605, 29, 130, 30);
        mainPagePanel.add(lblCurrentProfit);

        setLocation(PositionUtils.getScreenCenterPoint(this));
    }
}
