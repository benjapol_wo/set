package gui_backend;

import gui_elements.*;

public class PageUtils {
    public static void startSplash() {
        DebugUtils.writeln("gui_backend.PageUtils | startSplash() | Starting Splash...");
        new SplashScreenFrame().run();
        DebugUtils.writeln("gui_backend.PageUtils | startSplash() | Splash done.");
    }

    public static void openMainPage(String username) {
        DebugUtils.writeln("gui_backend.PageUtils | openMainPage() | Opening MainPage with username = \"" + username + "\"...");
        MainPageFrame.username = username;
        MainPageFrame.main(new String[0]);
    }

    public static void openLoginPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openLoginPage() | Opening LoginPage...");
        LoginPageFrame.main(new String[0]);
    }

    public static void quitApp(int status) {
        if (status == -2) {
            DebugUtils.writeln("gui_backend.PageUtils | quitApp() | exiting due to security breach (-2)");
        }
        if (status == 1) {
            DebugUtils.writeln("gui_backend.PageUtils | quitApp() | exiting without saving data (1)");
        }
        if (status == 0) {
            DebugUtils.writeln("gui_backend.PageUtils | quitApp() | normal exit, saving data...");
            StorageUtils.saveAllData();
        }
        DebugUtils.writeln("gui_backend.PageUtils | quitApp() | EXITING... with status = \"" + status + "\" (System.exit(" + status + "))");
        if (DebugUtils.logger != null) {
            DebugUtils.logger.close();
        }
        System.exit(status);
    }

    public static void openRegisterPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openRegisterPage() | Opening RegisterPage...");
        RegisterPageFrame.main(new String[0]);
    }

    public static void openSettingsPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openSettingsPage() | Opening SettingsPage...");
        SettingsPageFrame.main(new String[0]);
    }

    public static void showInfoDialog(int dialogType, String message) {
        DebugUtils.writeln("gui_backend.PageUtils | showInfoDialog() | Creating dialog box with dialogType = " + dialogType + " and message = \"" + message + "\"");
        InfoDialogFrame.main(dialogType, message);
    }

    public static void openCustomerPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openCustomerPage() | Opening CustomerPage...");
        CustomerPageFrame.main(new String[0]);
    }

    public static void openAddCustomerPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openAddCustomerPage() | Opening AddCustomerPage...");
        AddCustomerPageFrame.main(new String[0]);
    }

    public static void openViewCustomerPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openViewCustomerPage() | Opening ViewCustomerPage...");
        ViewCustomerPageFrame.main(new String[0]);
    }

    public static void openCourtPage(int courtNumber) {
        DebugUtils.writeln("gui_backend.PageUtils | openCourtPage() | Opening CourtPage for Court#" + courtNumber + "...");
        CourtPageFrame.courtNumber = courtNumber;
        CourtPageFrame.main(new String[0]);
    }

    public static void openAddBookingPage(int courtNumber, int periodIndex) {
        DebugUtils.writeln("gui_backend.PageUtils | openAddBookingPage() | Opening AddBookingPage for Court#" + courtNumber + " at Period#" + periodIndex + "...");
        AddBookingPageFrame.courtNumber = courtNumber;
        AddBookingPageFrame.periodIndex = periodIndex;
        AddBookingPageFrame.main(new String[0]);
    }

    public static void openAddBookPage(int courtNumber, int periodIndex, int payStatus) {
        DebugUtils.writeln("gui_backend.PageUtils | openAddBookPage() | Opening AddBookPage for Court#" + courtNumber + " at Period#" + periodIndex + "...");
        AddBookPageFrame.courtNumber = courtNumber;
        AddBookPageFrame.periodIndex = periodIndex;
        AddBookPageFrame.payStatus = payStatus;
        AddBookPageFrame.main(new String[0]);
    }

    public static void openBookingPage() {
        DebugUtils.writeln("gui_backend.PageUtils | openBookingPage() | Opening BookingPage...");
        BookingPageFrame.main(new String[0]);
    }
}
