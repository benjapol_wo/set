package gui_backend;

import appengine_classes.Booking;

import java.util.ArrayList;

public class BookingUtils {
    public static final int STATUS_PAID = 0, STATUS_UNPAID = -1;
    public static ArrayList<Booking> bookings = new ArrayList<Booking>();

    public static int book(int customerIndex, int courtNumber, int periodIndex, int status) {
        DebugUtils.writeln("gui_backend.BookingUtils | book() -- trying booking with arguments [" + customerIndex + ", " + courtNumber + ", " + periodIndex + ", " + status);
        try {
            if (CourtUtils.courts.get(courtNumber - 1).getPeriods().get(periodIndex).booked) {
                DebugUtils.writeln("gui_backend.BookingUtils | book() -- Period booked.");
                return -2;
            } else {
                bookings.add(new Booking(CustomerUtils.customers.get(customerIndex), CourtUtils.courts.get(courtNumber - 1), periodIndex, status));
                CourtUtils.courts.get(courtNumber - 1).getPeriods().get(periodIndex).booked = true;
                DebugUtils.writeln("gui_backend.BookingUtils | book() -- Booking successful.");
                return 0;
            }
        } catch (Exception e) {
            DebugUtils.writeln("gui_backend.BookingUtils | book() -- Booking failed. Exception occured.");
            e.printStackTrace();
            return -1;
        }
    }

    public static double getProfit() {
        double expense = 400 * 8;
        double revenue = 0;
        for (int i = 0; i < bookings.size(); i++) {
            if (bookings.get(i).getStatus() == STATUS_PAID) {
                revenue += 300;
            }
        }
        return revenue - expense;
    }

    public static String getViewMode() {
        //bookings.add(new Booking(new Customer("HELLO", "ABC", "F"), CourtUtils.courts.get(0), 1, STATUS_PAID));
        String s = "";
        s += String.format("%s\t| %s\t| %s\t| %s\t| %s\t", "bookID", "Customer", "Court", "Period", "Status");
        for (int i = 0; i < bookings.size(); i++) {
            s += String.format("%04d\t| %s\t| %s\t| %s\t| %s\t", i, (bookings.get(i).getCustomer().getName() + " " + bookings.get(i).getCustomer().getLastname()), CourtUtils.courts.indexOf(bookings.get(i).getCourt()), CourtUtils.getDefaultPeriods().get(bookings.get(i).getPeriodIndex()).startTime.toString().substring(0, 6) + CourtUtils.getDefaultPeriods().get(bookings.get(i).getPeriodIndex()).endTime.toString().substring(0, 6), bookings.get(i).getStatus());
        }
        return s;
    }
}
