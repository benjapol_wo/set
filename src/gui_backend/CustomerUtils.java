package gui_backend;

import appengine_classes.Customer;

import java.util.ArrayList;

public class CustomerUtils {
    public static ArrayList<Customer> customers = new ArrayList<Customer>();

    public static int add(String name, String lastname, String gender) {
        DebugUtils.writeln("gui_backend.CustomerUtils | add() -- trying to create a new customer " + name + " " + lastname + " " + gender);
        if (name.contains("=") || lastname.contains("=")) {
            DebugUtils.writeln("gui_backend.CustomerUtils | add() -- Error creating, customer cannot have \"=\" in name or lastname");
            return -2;
        } else if (name.equals("") || lastname.equals("")) {
            DebugUtils.writeln("gui_backend.CustomerUtils | add() -- Error creating, illegal name and/or lastname");
            return -1;
        } else {
            customers.add(new Customer(name, lastname, gender));
            DebugUtils.writeln("gui_backend.CustomerUtils | add() -- Customer " + name + " " + lastname + " successfully added to list.");
            return 0;
        }
    }

    public static String getViewMode() {
        DebugUtils.writeln("gui_backend.CustomerUtils | getViewMode() -- Generating ViewMode String Format...");
        String s = "";
        s += String.format("%s\t| %s\t| %s\t| %s\n", "ID", "M/F", "Name", "Lastname");
        for (int i = 0; i < customers.size(); i++) {
            s += String.format("%04d\t| %s\t| %s\t| %s\n", i, customers.get(i).getGender(), customers.get(i).getName(), customers.get(i).getLastname());
        }
        return s;
    }

}
