package gui_backend;

public class SettingsUtils {
    public static void clearAllData() {
        DebugUtils.writeln("gui_backend.SettingsUtils | clearAllData() -- Clearing all data...");
        clearCredentialData();
        clearCourtData();
        clearCustomerData();
        clearBookingData();
    }

    public static void clearCredentialData() {
        CredentialUtils.credentials.clear();
        DebugUtils.writeln("gui_backend.SettingsUtils | clearCredentialData() -- Users credentials cleared.");
    }

    public static void clearCourtData() {
        CourtUtils.courts.clear();
        DebugUtils.writeln("gui_backend.SettingsUtils | clearCourtData() -- Courts cleared.");
    }

    public static void clearCustomerData() {
        CustomerUtils.customers.clear();
        DebugUtils.writeln("gui_backend.SettingsUtils | clearCustomerData() -- Customers cleared.");
    }

    public static void clearBookingData() {
        BookingUtils.bookings.clear();
        DebugUtils.writeln("gui_backend.SettingsUtils | clearBookingData() -- Bookings cleared.");
    }
}
