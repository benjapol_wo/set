package gui_backend;

import appengine_classes.Customer;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StorageUtils {
    public static void loadAllData() {
        DebugUtils.writeln("gui_backend.StorageUtils | loadAllData() | Loading data from storage...");
        loadCredentialData();
        loadCustomerData();
    }

    public static void saveAllData() {
        DebugUtils.writeln("gui_backend.StorageUtils | saveAllData() | Saving data to storage...");
        saveCredentialData();
        saveCustomerData();
    }

    public static void saveCredentialData() {
        try {
            File toWrite = new File("src/storage/credStore.dat");
            DebugUtils.writeln("gui_backend.StorageUtils | saveCredentialData() | Saving to file " + toWrite.getAbsolutePath() + "...");
            PrintWriter writer = new PrintWriter(new FileWriter(toWrite));
            ArrayList<String[]> cred = CredentialUtils.credentials;
            for (int i = 0; i < cred.size(); i++) {
                writer.printf("%s=%s=", cred.get(i)[0], cred.get(i)[1]);
                if (i < cred.size() - 1) {
                    writer.println();
                }
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadCredentialData() {
        try {
            File toRead = new File("src/storage/credStore.dat");
            DebugUtils.writeln("gui_backend.StorageUtils | loadCredentialData() | Loading from file " + toRead.getAbsolutePath() + "...");
            Scanner scan = new Scanner(toRead);
            ArrayList<String[]> cred = CredentialUtils.credentials;
            while (scan.hasNextLine()) {
                String[] reader = scan.nextLine().split("=");
                cred.add(new String[]{reader[0], reader[1]});
            }
            scan.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveCustomerData() {
        try {
            File toWrite = new File("src/storage/customerStore.dat");
            DebugUtils.writeln("gui_backend.StorageUtils | saveCustomerData() | Saving to file " + toWrite.getAbsolutePath() + "...");
            PrintWriter writer = new PrintWriter(new FileWriter(toWrite));
            ArrayList<Customer> cust = CustomerUtils.customers;
            for (int i = 0; i < cust.size(); i++) {
                writer.printf("%s=%s=%s=", cust.get(i).getName(), cust.get(i).getLastname(), cust.get(i).getGender());
                if (i < cust.size() - 1) {
                    writer.println();
                }
            }
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadCustomerData() {
        try {
            File toRead = new File("src/storage/customerStore.dat");
            DebugUtils.writeln("gui_backend.StorageUtils | loadCustomerData() | Loading from file " + toRead.getAbsolutePath() + "...");
            Scanner scan = new Scanner(toRead);
            while (scan.hasNextLine()) {
                String[] reader = scan.nextLine().split("=");
                CustomerUtils.add(reader[0], reader[1], reader[2]);
            }
            scan.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
