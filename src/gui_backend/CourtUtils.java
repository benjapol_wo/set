package gui_backend;

import appengine_classes.Court;
import appengine_classes.Period;
import appengine_classes.Time;

import java.util.ArrayList;

public class CourtUtils {
    public static ArrayList<Court> courts = new ArrayList<Court>();
    private static final ArrayList<Period> DEFAULT_PERIODS = new ArrayList<Period>();

    public static void initialize() {
        DEFAULT_PERIODS.add(new Period(new Time(8, 0, 1), new Time(9, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(9, 0, 1), new Time(10, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(10, 0, 1), new Time(11, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(11, 0, 1), new Time(12, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(12, 0, 1), new Time(13, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(13, 0, 1), new Time(14, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(14, 0, 1), new Time(15, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(15, 0, 1), new Time(16, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(16, 0, 1), new Time(17, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(17, 0, 1), new Time(18, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(18, 0, 1), new Time(19, 0, 0)));
        DEFAULT_PERIODS.add(new Period(new Time(19, 0, 1), new Time(20, 0, 0)));
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
        courts.add(new Court());
    }

    @SuppressWarnings("unchecked")
    public static ArrayList<Period> getDefaultPeriods() {
        return (ArrayList<Period>) DEFAULT_PERIODS.clone();
    }

    public static String getStatusString(int courtNumber, int periodIndex) {
        DebugUtils.writeln("gui_backend.CourtUtils | getStatusString() -- getting status for Court#" + courtNumber + " period " + periodIndex);
        if (courts.get(courtNumber - 1).getPeriods().get(periodIndex).booked) {
            return "booked";
        } else {
            return "available";
        }
    }
}
