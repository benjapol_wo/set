package appengine_classes;

public class Period {
    public Time startTime, endTime;
    public boolean booked;

    public Period(Time startTime, Time endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.booked = false;
    }
}
