package appengine_classes;

public class Customer {
    private String name, lastname, gender;
    public static int count = 0;

    public Customer(String name, String lastname, String gender) {
        this.name = name;
        this.lastname = lastname;
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String toString() {
        return String.format("%d=%s=%s=%s", name, lastname, gender);
    }
}
