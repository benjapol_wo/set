package appengine_classes;

import gui_backend.CourtUtils;

import java.util.ArrayList;

public class Court {
    private ArrayList<Period> periods;

    public Court() {
        periods = CourtUtils.getDefaultPeriods();
    }

    public ArrayList<Period> getPeriods() {
        return periods;
    }
}
