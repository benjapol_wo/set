package appengine_classes;

public class Booking {
    public static final int STATUS_PAID = 0, STATUS_UNPAID = -1;
    private Customer customer;
    private Court court;
    private int periodIndex;
    private int status;

    public Booking(Customer customer, Court court, int periodIndex, int status) {
        this.customer = customer;
        this.court = court;
        this.status = status;
        this.periodIndex = periodIndex;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCourt(Court court) {
        this.court = court;
    }

    public Court getCourt() {
        return court;
    }

    public void setPeriodIndex(int periodIndex) {
        this.periodIndex = periodIndex;
    }

    public int getPeriodIndex() {
        return periodIndex;
    }

}
